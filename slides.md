# Wzorce projektowe G4



# Geneza
> "Wzorzec opisuje problem, który powtarza się wielokrotnie w danym środowisku, oraz podaje istotę jego rozwiązania w taki sposób, aby można było je zastosować miliony razy bez potrzeby powtarzania tej samej pracy"

Christopher Alexander "A pattern language", 1977


# Motywacja

* Czy odpowiedzi na powtarzające się pytania można przedstawić w sposób ogólny, tak aby były pomocne w tworzeniu rozwiązań w różnych konkretnych kontekstach?
* Czy jakość projektu można opisać w kategoriach powtarzalnych rozwiązań, bez konieczności ciągłego "wynajdywania koła"?


# Historia

* Architektura - **NO**
* Informatyka - **YES**
  * 1987 - konferencja OOPSLA Kent Beck, Warda Cunninghama
  * 1995 - książka Bandy Czworga (Erich Gamma. Richard Helm, Ralph johnson, John Vlissides)


# Definicja I

> Wzorzec to sprawdzona koncepcja, która opisuje problem powtarzający się wielokrotnie w określonym kontekście, działające na niego siły, oraz podaje istotę jego rozwiązania w sposób abstrakcyjny

Christopher Alexander


# Definicja II

> Wzorzec projektowy identyfikuje i opisuje pewną abstrakcję, której poziom znajduje się powyżej poziomu abstrakcji pojedynczej klasy, instancji lub komponentu.

E. Gamma, R. Helm, R. Johnson, J. Vlissides (1994)


# Podział wzorców projektowych

![Podział wzorców](http://brasil.cel.agh.edu.pl/~09sbfraczek/images/wzorce/design.jpg)


# Szablon wzorca projektowego

![szablon wzorca](http://brasil.cel.agh.edu.pl/~09sbfraczek/images/wzorce/3.png)



# Wzorce konstrukcyjne



## Singleton


#### Przeznaczenie
* ograniczanie możliwości tworzenia obiektów danej klasy do jednej instancji
* zapewnienie globalnego punktu dostępu
* zamiast obiektu globalnego


#### Waunki stosowania
* konieczność posiadania wyłącznie jednej instancji


#### Struktura
![singleton](http://upload.wikimedia.org/wikipedia/commons/b/bd/Singleton.png)


#### Konsekwencje
* kontrolowany dostęp do jedynej instancji
* redukowana przestrzeń nazw
* pozwala na zwiększenie dozwolonej liczbny instancji
* większa elastyczność od operacji statycznych


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/creational/singleton/)


#### Przykłady
* manadżer okien
* punkt dostępu do baz danych
* kolejka do jednej drukarki
* [java.lang.Runtime](http://docs.oracle.com/javase/8/docs/api/java/lang/Runtime.html#getRuntime%28%29)
* [java.awt.Desktop](http://docs.oracle.com/javase/8/docs/api/java/awt/Desktop.html#getDesktop%28%29)


#### Powiązania
* implementacja innych wzorów, np. fabryki, budowniczego



## Prototyp


#### Przeznaczenie
* kopiowanie instancji już istniejących i modyfikowanie ich, zamiast tworzenia całkiem nowych instancji


#### Warunki stosowania
* system powinien być niezależny od tego, jak jego produkty są tworzone, składane i reprezentowane
* klasy, których egzemplarze należy tworzyć są specyfikowane w czasie wykonywania programu, np. przez dynamiczne ładowanie
* potrzeba uniknięcia budowania hierarchii klas fabryk, która jest porównywalna z hierarchią klas produktów
* stan obiektów klasy może przyjmować tylko jedną z kilku różnych wartości; może być wówczas wygodniej zainstalować odpowiednią liczbę prototypów i klonować je niż ręcznie tworzyć egzemplarze klasy za każdym razem z odpowiednim stanem


#### Struktura
![prototyp](http://upload.wikimedia.org/wikipedia/commons/1/14/Prototype_UML.svg)


#### Elementy
* __Prototype__ - deklaruje interfejs klonowania
* __ConcretePrototype__ - implementuje operację klonowania
* __Client__ - tworzy nowy obiekt, prosząc prototyp o sklonowanie się


#### Współdziałanie
* klient prosi prototyp o sklonowanie się


#### Konsekwencje
* pozwala na dodawanie i usuwanie obiektów w czasie przebiegu programu
* możliwość tworzenia nowych obiektów przez zmianę wartości
* możliwość tworzenia nowych obiektów przez zmianę struktury
* zmniejsze liczby podklas
* przyspiesza kreowanie dużych obiektów


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/creational/prototype/)


#### Przykłady
* [java.lang.Cloneable](http://docs.oracle.com/javase/8/docs/api/java/lang/Cloneable.html)


#### Powiązania
* konkurancja dla fabryki
* czasem używane wraz z kompozytem lub dekoratorem



## Budowniczy


#### Przeznaczenie
* oddzielenie tworzenia złożonego obiektu od jego reprezentacji
* ten sam proces konstrukcji może prowadzić do powstania różnych reprezentacji


#### Warunki stosowania
* algorytm tworzeniq obiektu złożonego powinin być niezależny od składników tego obiektu i sposobów ich łączenia
* proces konstrukcji musu umożliwiać tworzenie różnych reprezentacji generowanego obiektu


#### Struktura
![builder](http://upload.wikimedia.org/wikipedia/commons/f/f3/Builder_UML_class_diagram.svg)


#### Elementy
* __Builder__ – określa interfejs abstrakcyjny do tworzenia składników obieku __Product__
* __ConcreteBuilder__ – tworzy i łączy składniki produktu w implementacji klasu __Builder__
* __Director__ – tworzy obiekt za pomocą interfejsu klasy __Builder__
* __Product__ – reprezentuje generowany obiekt złożony


#### Współdziałanie
* klient tworzy obiekt __Director__ i konfiguruje go za pomocą odpowiedniego obiektu __Builder__
* __Director__ wysyła powiadomienie do obiektu __Builder__
* __Builder__ obsługuje żądania od obiektu __Director__ i dodaje części do produktu
* klient pobiera produkt od obiektu __Builder__


#### Konsekwencje
* możliwość modyfikowania wewnętrznej reprezentacji produktu
* odizolowanie reprezentacji od kodu służącego do tworzenia poduktu
* większa kontrola nad procesem tworzenia


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/creational/builder/)


#### Przykłady
* parsery
* [java.lang.StringBuilder](http://docs.oracle.com/javase/8/docs/api/java/lang/StringBuilder.html)
* [java.swing.GroupLayout](http://docs.oracle.com/javase/8/docs/api/javax/swing/GroupLayout.html)
* [com.google.common.collect.MapMaker](http://docs.guava-libraries.googlecode.com/git/javadoc/com/google/common/collect/MapMaker.html)
* [groovy.xml.MarkupBuilder](http://groovy.codehaus.org/api/groovy/xml/MarkupBuilder.html)


#### Powiązania
 * fabryka tworzy rodzinę obiektów i zwraca ją od razu
 * budowniczy tworzy zlożony obiekt i zwraca go w ostatnim kroku
 * często używany do tworzenia kompozytów



## Fabryka abstrakcyjna


#### Przeznaczenie
* tworzenie rodzin powiązanych ze sobą lub zależnych od siebie obiektów bez określania ich konkretnych klas


#### Warunki stosowania
* niezależność od sposobu tworzenia, składania i reprezentowania produktów
* konfuguracja za pomocą jednej z wielu rodzin produktów
* powiązane obiekty z jednej rodziny muszą razem współdziałać (a nie obiekty z różnych rodzin)
* udostępnienie klasy bibliotecznej produktów bez ujawniania implementacji


#### Struktura
![fabryka abstrakcyjna](http://upload.wikimedia.org/wikipedia/commons/9/9d/Abstract_factory_UML.svg)


#### Elementy
* __AbstractFactory__ – deklaracja interfejsu z operacjami tworzącymi produkty abstrakcyjne
* __ConcreteFactory__ – implementacja operacji tworzących produkty konkretne
* __AbstractProduct__ – deklaracja interfejsu dla produktów określonego typu
* __ConcreteProduct__ – obiekt produktu tworzony przez odpowiadającą mu fabrykę konkretną
* __Client__ – korzysta jedynie z interfejsów __AbstractFactory__ i __AbstractProduct__


#### Współdziałanie
* zwykle jeden egzemplarz __ConcreteFactory__ tworzącej produkty o określonej implementacji
* rózne produkty są generowane przez różne fabryki konkretne
* __AbstractFactor__ przekazuje tworzenie produktów do swojej podklasy __ConcreteFactory__


#### Konsekwencje
* izoluje klasy konkretne
* ułatwia zastępowanie rodzin produktów
* ułatwia zaczhowanie spójności między produktami
* ułatwia dodawanie obsługi produktów nowego rodzaju


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/creational/abstractfactory/)


#### Przykłady
* [javax.xml.parsers.DocumentBuilderFactory](http://docs.oracle.com/javase/8/docs/api/javax/xml/parsers/DocumentBuilderFactory.html#newInstance%28%29)
* [javax.xml.transform.TransformerFactory](docs.oracle.com/javase/8/docs/api/javax/xml/transform/TransformerFactory.html#newInstance%28%29)
* [javax.xml.xpath.XPathFactory](http://docs.oracle.com/javase/8/docs/api/javax/xml/xpath/XPathFactory.html#newInstance%28%29)


#### Powiązania
* często implementowane za pomocą metod wytwórczych
* zazwyczaj są singletonami



## Metoda wytwórcza


#### Przeznaczenie
* przekazanie procesu tworzenia egzemplarzy podklasom


#### Warunki stosowania
* nie można z góry ustalić klasy obiektów, które trzeba utworzyć
* podklasy danej klasy mają określać tworzone przez nią obiekty
* delegowanie zadań do jednej z kilku podklas pomocniczych


#### Struktura
![metoda fabrykująca](http://upload.wikimedia.org/wikipedia/commons/a/a3/FactoryMethod.svg)


#### Elementy
* __Product__ – interfejs obiektów generowanych przez metodę wytwórczą
* __ConcreteProduct__ – implementacja produktu
* __Creator__ – zawiera deklarację metody wytwórczej zwracającej obiekty typu __Product__
* __ConcreteCreator__ – implementuje metodę wytwórczą tak, że zwraca egzemplarz klasy __ConcreteProduct__


#### Współdziałanie
* __Creator__ działa na podstawe założenia, że w jej podklasach zdefiniowana jest metoda wytwórcza zwracająca egzemplarz odpowiedniej klasy __ConcreteProduct__


#### Konsekwencje
* eliminacja konieczności wiązania specyficznych klas z kodem
* zapewnie punktów zaczepienia dla podklas
* połączenie równoległych hierarchii klas


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/creational/factorymethod)


#### Przykłady
* [java.util.Calendar](http://docs.oracle.com/javase/8/docs/api/java/util/Calendar.html#getInstance%28%29)
* [java.util.ResourceBundle](http://docs.oracle.com/javase/8/docs/api/java/util/ResourceBundle.html#getBundle%28java.lang.String%29)
* [java.text.NumberFormat](http://docs.oracle.com/javase/8/docs/api/java/text/NumberFormat.html#getInstance%28%29)
* [java.nio.charset.Charset](http://docs.oracle.com/javase/8/docs/api/java/nio/charset/Charset.html#forName%28java.lang.String%29)


#### Powiązania
* często używany do implementacji fabryk



# Wzorce strukturalne



## Adapter / wrapper


#### Przeznaczenie
* przeksztalcenie interfejsu klasy na inny oczekiwany przez klienta
* umozliwienie współpracy między klasami z niedopasowanymi interfejsami


#### Warunki stosowania
* wykorzystanie klasy z niedopasowanym interfejsem
* klasa do wielokrotnego użytku współdziałająca z niepowiązanymi lub nieznanymi klasami


#### Struktura
![adapor klasowy](http://upload.wikimedia.org/wikipedia/commons/3/35/ClassAdapter.png)
![adaptor obiektowy](http://upload.wikimedia.org/wikipedia/commons/d/d7/ObjectAdapter.png)


#### Elementy
* __Target__ – definiuje specyficzny dla dziedziny interfejs używany przez klienta
* __Client__ – współdziała z obiektami zgodnymi z interfejsem klasy __Target__
* __Adaptee__ – definiuje istniejący interfejs, który trzeba dostosować
* __Adaptor__ – dostosowuje interfejs klasy __Adaptee__ do interfejsu klasy __Target__


#### Współdziałanie
* klienty wywołują operacje egzemplarzy klasy __Adaptee__
* adapter wywołuje operacje klasy __Adaptee__, które obsługują żądanie


#### Konsekwencje

###### Adapter klasowy
* adapter nie zadziała przy dostosowaniu klasy oraz wszystkich jej podklas
* umożliwienie przesłonięcia wybranych operacji z __Adaptee__ (ze względu na bycie jej podklasą)
* dodanie tylko jednego obiektu

###### Adapter obiektowy
* umożliwienie współdziałania jednej klasy adaptera z wieloma klasami __Adaptee__ (również jej podklasami)
* utrudnia przesłanianie zachowań z klasy __Adaptee__


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/structural/)


#### Przykłady
* [java.util.Arrays#asList()](http://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html#asList%28T...%29)
* [java.io.InputStreamReader(InputStream)](http://docs.oracle.com/javase/8/docs/api/java/io/InputStreamReader.html#InputStreamReader-java.io.InputStream-)
* [java.io.OutputStreamWriter(OutputStream)](http://docs.oracle.com/javase/8/docs/api/java/io/OutputStreamWriter.html#OutputStreamWriter-java.io.OutputStream-)


#### Powiązania
* most ma podobną strukturę do adaptera obiiektowego, ale pełni inne funkcje (rozdziela interfejs od implementacji)
* dekorator jest bardziej przezroczysty niż adapter, bo nie zmienia interfejsu, umożliwia w związku z tym składanie rekurencyjne
* pełnomocnik definuje substytut obiektu, a nie modyfikuje jego interfejs



## Dekorator


#### Przeznaczenie
* dynamiczne dołączanie dodatkowych funkcjonalności do obiektu
* alternatywny sposób tworzenia podklas o wzbogaconej funkcjonalności


#### Warunki stosowania
* dodawanie zadań do poszczególnych obiektów w sposób dynamiczny i przezroczysty
* mechanizm obsługi zadań, które można cofnąć
* niepraktyczne rozszerzanie przez tworzenie podklas (np. duża liczba rozszerzeń)


#### Struktura
![dekorator](http://upload.wikimedia.org/wikipedia/commons/e/e9/Decorator_UML_class_diagram.svg)


#### Elementy
* __Component__ – interfejs obiektów, do których można dynamicznie dodawać obsługę zadań
* __ConcreteComponent__ – obiekt, do którego można dynamicznie dodawać obsługę zadań
* __Decorator__ – przechowuje referencję do obiektu __Component__ i definiuje interfejs zgodny z __Component__
* __ConcreteDecorator__ – dodaje zadania do komponentu


#### Współdziałanie
* __Decorator__ przekazuje żądania do powiązanego z nim obiektu __Component__
* opcjonalne wykonywanie dodatkowych operacji


#### Konsekwencje
* zapewnia większą elastyczność niż statyczne dziedziczenie
* pozwala uniknąć tworzenia przeładowanych funkcjami klas na wysokich poziomach hierarchii
* dekorator i powiązany z nim komponent nie są identyczne
* powstawanie wielu małych obiektów


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/structural/decorator/)


#### Przykłady
* podklasy [java.io.InputStream](http://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html), [java.io.OutputStream](http://docs.oracle.com/javase/8/docs/api/java/io/OutputStream.html), [java.io.Reader](http://docs.oracle.com/javase/8/docs/api/java/io/Reader.html), [java.io.Writer](http://docs.oracle.com/javase/8/docs/api/java/io/Writer.html)
* [java.util.Collections](http://docs.oracle.com/javase/8/docs/api/java/util/Collections.html) – meotdy _checkedXXX()_, _synchronizedXXX()_, _unmodifiableXXX()_


#### Powiązania
* w przeciwieństwie do adaptera modyfikuje jedynie zadania obiektu a nie jego interfejs
* wyglada jak uproszczony kompozyt (z jednym dzieckiem), ale przeznaczony jest do modyfikacji zachowań a nie przechowywania
* podobny do strategii, ale tam chodzi o modyfikację mechanizmów



## Fasada


#### Przeznaczenie
* dostarczenie jednolitego i uproszczonego zestawu interfejsów z danego podsystemu
* opisuje interfejs wyższego rzędu, który sprawia, że podsystem jest łatwiejszy w użyciu


#### Warunki stosowania
* dostarczanie prostszego interfejsu do złożonego podsystemu
* istnienie szereg zależności pomiędzy klientem a klasami implementacji abstrakcji
* wprowadzenie warstw do systemu


#### Struktura
![fasada](http://upload.wikimedia.org/wikipedia/en/5/57/Example_of_Facade_design_pattern_in_UML.png)


#### Elementy
* __Facade__ - wie jakie klasy podsystemu są odpowiedzialne za spełnienie żądania; przekazuje żądania klienta do odpowiednich obiektów podsystemu
* klasy podsystemu – implementują funkcje podsystemu; wykonują pracę przydzieloną przez obiekt __Facade__; nic nie wiedzą o fasadzie, to znaczy nie przechowują żadnych odwołań do niej


#### Współdziałanie
* klienci komunikują się z podsystemem, wysyłając żądania do obiektu __Facade__, który przekazuje je do odpowiednich obiektów podsystemu
* __Facade__ może być zmuszona realizować swoje zadania, polegające na tłumaczeniu jej interfejsu na interfejsy podsystemu
* klienci wykorzystujący fasadę nie muszą mieć bezpośredniego dostępu do obiektów jej podsystemu


#### Konsekwencje
* dostarcza prostszy interfejs do rozbudowanego podsystemu bez ograniczania jego funkcjonalności
* osłania klienta od złożoności komponentów podsystemu
* dostarcza "łącznik" pomiędzy podsystem a jego klientów
* ogranicza łączność pomiędzy podsystemami – każdy podsystem używa własnej fasady i inne części systemu używają wzorca fasady do komunikowania sie z podsystemami


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/structural/facade/)


#### Przykłady
* [javax.faces.context.FacesContext](http://docs.oracle.com/javaee/7/api/javax/faces/context/FacesContext.html)
* [javax.faces.context.ExternalContext](http://docs.oracle.com/javaee/7/api/javax/faces/context/ExternalContext.html)


#### Powiązania
* można go używać w powiązaniu z fabryką do tworzenia obiektów podsystemu niezależnie od podsystemów
* czasem stosuje się fabrykę zamiast fasady do ukrycia klas specyficznych dla platformy
* podobny jest mediator, ale służy do ogarnięcia komunikacji między obiektami, a nie dostarczenia uproszczonego interfejsu do podsystemów
* zwykle potrzebny jest jeden obiekt fasady – singleton



## Most


#### Przeznaczenie
* oddzielenie interfejsu od implementacji tak by oba elementy mogły istnieć niezależnie, a co za tym idzie by powstała możliwość wprowadzania zmian do implementacji bez konieczności zmian w kodzie, który korzysta z klasy


#### Warunki stosowania
* wymagana jest oddzielność interfejsu od implementacji
* zarówno interfejs jak i implementacja musi zostać rozbudowana poprzez podklasy
* zmiana implementacyjne nie mogą mieć wpływu na klienta


#### Struktura
![most](http://upload.wikimedia.org/wikipedia/commons/c/cf/Bridge_UML_class_diagram.svg)


#### Elementy
* __Abstraction__ – definiuje interfejs abstrakcji i przechowuje referencję do obiektu typu __Implementor__
* __RefinedAbstraction__ – rozszerza interfejs zdefiniowany przez __Abstraction__
* __Implementor__ – definiuje interfejs klasy implementacji
* __ConcreteImplementor__ – implementuje interfejs __Implementor__


#### Współdziałanie
* klasa __Abstraction__ przekazuje żądania klienta do powiązanego z nią obiektu __Implementor__


#### Konsekwencje
* umożliwia odseparowanie implementacji od interfejsu.
* poprawia możliwość rozbudowy klas
* ukrywanie szczegółów implementacyjnych od klienta


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/structural/bridge/)


#### Przykłady
* ...


#### Powiązania
* do utworzenia i skonfigurowania konkretnego mostu można użyć fabryki
* adapter umożliwia współdziałanie między niepowiązanymi klasami, zaś most do niezależnego modyfikowania abstrakcji i implementacji



## Pełnomocnik


#### Przeznaczenie
* potrzeba bardziej uniwersalnego lub bardziej wyrafinowanego odwołania do obiektu niż zwykły wskaźnik
* kontrolowanie dostepu do obiektu


#### Warunki stosowania
* wirtualny - przechowuje obiekty, których utworzenie jest kosztowne; tworzy je na żądanie
* zabezpieczający - kontroluje dostęp do obiektu sprawdzając, czy obiekt wywołujący ma odpowienie prawa do obiektu wywoływanego
* zdalny - czasami nazywany ambasadorem; reprezentuje obiekty znajdujące się w innej przestrzeni adresowej
* nteligentna referencja - czasami nazywany sprytnym wskaźnikiem; pozwala na wykonanie dodatkowych akcji podczas dostępu do obiektu, takich jak: zliczanie referencji do obiektu czy ładowanie obiektu do pamięci


#### Struktura
![pośrednik](http://upload.wikimedia.org/wikipedia/commons/7/75/Proxy_pattern_diagram.svg)


#### Elementy
* __Proxy__ – przechowuje odwołanie, które umożliwia mu dostęp do __RealSubject__; może odwoływać się do przedmiotu, jeśli interfejsy __Subject__ i __RealSubject__ są takie same; zapewnia taki sam interfejs jak interfejs __Subject__, tak że __Proxy__ może być zastąpiony przez __Subject__; kontroluje dostęp do __RealSubject__ i może być odpowiedzialny za tworzenie oraz usuwanie tego przedmiotu;
  * pełnomocnicy zdalni są odpowiedzialni za kodowanie żądań i ich argumentów oraz za wysyłanie zakodowanych żądań do rzeczywistych przedmiotów w innej przestrzeni adresowej
  * pełnomocnicy wirtualni mogą przechowywać w pamięci podręcznej dodatkowe informacje o rzeczywistym przedmiocie, dzięki czemu mogą odłożyć na później uzyskanie dostępu do niego
  * pełnomocnicy ochraniający sprawdzają, czy wywołujący ma zezwolenie na dostęp, wymagane na spełnienie danego żądania
* __Subject__ – definiuje wspólny interfejs dla __RealSubject__ i __Proxy__, tak żeby __Proxy__ mógł być używany wszędzie tam, gdzie jest oczekiwany __RealSubject__
* __RealSubject__ – definiuje rzeczywisty obiekt reprezentowany przez pełnomocnika


#### Współdziałanie
* __Proxy__, jeśli trzeba przekazuje żądania do __RealSubject__


#### Konsekwencje
* pośrednik zdalny może ukrywać fakt, że znajduje się w innej przestrzeni adresowej
* pośrednik wirtualny może umożliwiać optymalizację, np. przez tworzenie obiektów na żądanie
* pełnomocnik zabezpieczający i inteligentna referencja umożliwia wykonanie dodatkowych operacji porządkowych przy dostępie do obiektu


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/structural/proxy/)


#### Przykłady
* [java.lang.reflect.Proxy](http://docs.oracle.com/javase/8/docs/api/java/lang/reflect/Proxy.html)
* [java.rmi.*](http://docs.oracle.com/javase/8/docs/api/java/rmi/package-summary.html)


#### Powiązania
* interfejs pełnomocnika może być podzbiorem interfejsu obiektu – podobieństwo do adaptera
* dekorator dodaje zadania do obiektu a pełnomocnik kontroluje dostęp do niego



## Pyłek


#### Przeznaczenie
* wykorzystanie współdzielenia do wydajnej obsługi dużej liczby małych obiektów


#### Warunki stosowania
* aplikacja korzysta z dużej liczby obiektów
* koszty przechowywania obiektów są wysokie z uwagi na ich liczbę
* większość stanu obiektu można zapisać poza nim
* po przeniesieniu stanu na zewnątrz wiele grup obiektów można zastąpić stosunkowo nielicznymi obiektami współużytkowanymi
* aplikacja nie jest zależna od tożsamości obiektów


#### Struktura
![pyłek](http://brasil.cel.agh.edu.pl/~09sbfraczek/images/wzorce/28.png)


#### Elementy
* __Flyweight__ – podlega współdzieleniu między klientów; definiuje interfejs do przyjmowania i odtwarzania stanu zewnętrznego obiektu; przechowuje stan wewnętrzny (współdzielony); jest niezależny od kontekstu (z wyjątkiem stanu zewnętrznego) 
* __FlyweightFactory__ – tworzy pyłki i zarząda nimi; gwarantuje, że pyłki są prawidłowo współużytkowane
* __Client__ – przechowuje referencję do pyłków; oblicza lub przechowuje zewnętrzny stan pyłków


#### Współdziałanie
* stan potrzebny pyłkom trzeba podzielić na wewnętrzny i zewnętrzny
* stat nwewnętrzny przechowywany jest przez __ConcreteFlyweight__ i zapisywany lub obliczany jest przez klienta (w momencie wywołania operacji)
* klienty nie powinny bezpośrednio tworzyć instancji __ConcreteFlyweight__; muszą je otzymywać z __FlyweightFactory__, co gwarantuje prawidłowe współużytkowanie


#### Konsekwencje
* zmniejszenie wymagań pamięciowych programu
  * zmniejszenie ogólnej liczby obiektów
  * zmniejszenie rozmiaru stanu obiektów
  * stan zewnętrzny może być przechowywany lub wyliczany
* wzrost złożoności obliczeniowej – dodatkowy nakład na zarządzanie stanem zewnętrznym


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/structural/flyweight/)


#### Przykłady
* [java.lang.Integer#valueOf(int)](http://docs.oracle.com/javase/8/docs/api/java/lang/Integer.html#valueOf-int-)


#### Powiązania
* cześto stosuje się go wraz z kompozytem do implementacji acyklicznego grafu skierowanego ze współużytkownaymi liśćmi
* obiekty stanu i strategii często najlepiej jest implementować jako pyłki



## Kompozyt


#### Przeznaczenie
* komponenty, które mogą być pojedyńczymy obiektami lub reprezentować heirarchię obiektów
* operowanie w sposób ogólny na obiektach które mogą (lub nie) reprezentować hierarchię obiektów


#### Warunki stosowania
* przedstawienie hierarchii obiektów typu cześć-całość
* ignorowanie różnic między złożeniami obiektów i pojedynczymi obiektami


#### Struktura
![kompozyt](http://upload.wikimedia.org/wikipedia/commons/5/5a/Composite_UML_class_diagram_%28fixed%29.svg)


#### Elementy
* __Component__ – deklaruje interfejs składanych obiektów; implementuje, tam gdzie to możliwe, domyślne zachowanie w wypadku interfejsu wspólnego dla wszystkich klas; definiuje interfejs umożliwiający dostęp i zarządzanie komponentami-dziećmi; czasami definiuje interfejs umożliwiający dostęp do rodzica komponentu w strukturze rekurencyjnej i implementuje go, o ile jest to potrzebne
* __Leaf__ – reprezentuje obiekty będące liśćmi w składanej strukturze; liść nie ma dzieci; definiuje zachowanie obiektów pierwotnych w strukturze
* __Composite__ – definiuje zachowanie komponentów mających dzieci; przechowuje komponenty będące dziećmi; implementuje operacje z interfejsu Komponentu związane z dziećmi
* __Client__ – manipuluje obiektami występującymi w strukturze, wykorzystując do tego interfejs komponentu


#### Współdziałanie
* klienci używają interfejsu z klasy __Component__ w celu komunikowania się z obiektami występującymi w składanej strukturze
* jeśli odbiorca jest obiektem __Leaf__, to żądania są realizowane bezpośrednio
* jeśli odbiorca jest obiektem __Composite__, to zwykle przekazuje żądania swoim komponentom-dzieciom, być może wykonując przed i/lub po przekazaniu dodatkowe operacje


#### Konsekwencje
* klient jednolicie wykonuje operacje na obiekcie złożonym i "prymitywnym"
* łatwo dodawać nowe rodzaje komponentów


#### Implementacja
* [przykład](https://bitbucket.org/mikus/dpjava/src/master/src/main/java/mo/designpatterns/structural/composite/)


#### Przykłady
* [java.awt.Container](http://docs.oracle.com/javase/8/docs/api/java/awt/Container.html)
* [javax.faces.component.UIComponent](http://docs.oracle.com/javaee/7/api/javax/faces/component/UIComponent.html)


#### Powiązania
* powiązania między komponentami i elementami nadrzędnymi często jest wykorzystywane w łańcuchu zobowiązań
* często wraz z kompozytem używany jest dekorator
* pyłek umożliwia współużytkownie komponentów, ale bez przechowywania referencji do elementów nadrzędnych
* do przechodzenia zawartości kompozytów można użyć wzorca iterator
* wzorzec odwiedzający zapewnia jedną lokalizację dla operacji i zachować, które inaczej są rozproszone po klasach kompozytów i liści



# Wzorce operacyjne

### C.D.N.


#### Przeznaczenie

#### Warunki stosowania

#### Struktura

#### Elementy

#### Współdziałanie

#### Konsekwencje

#### Implementacja

#### Przykłady



