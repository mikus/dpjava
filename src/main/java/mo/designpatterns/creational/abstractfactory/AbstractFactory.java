package mo.designpatterns.creational.abstractfactory;


public interface AbstractFactory {

    AbstractProductA createProductA();

    AbstractProductB createProductB();

}


interface AbstractProductA { }

abstract class AbstractProductB {

    AbstractProductA productA;

    void setProductA(AbstractProductA productA) {
        this.productA = productA;
    }

}


class ConctreteProductA1 implements AbstractProductA { }

class ConctreteProductA2 implements AbstractProductA { }

class ConctreteProductB1 extends AbstractProductB { }

class ConctreteProductB2 extends AbstractProductB { }


class ConcreteFactory1 implements AbstractFactory {

    @Override
    public AbstractProductA createProductA() {
        return new ConctreteProductA1();
    }

    @Override
    public AbstractProductB createProductB() {
        return new ConctreteProductB1();
    }
}


class ConcreteFactory2 implements AbstractFactory {

    @Override
    public AbstractProductA createProductA() {
        return new ConctreteProductA2();
    }

    @Override
    public AbstractProductB createProductB() {
        return new ConctreteProductB2();
    }
}


class Client {

    void construct(int type) {
        AbstractFactory factory = null;

        if (type == 1)
            factory = new ConcreteFactory1();
        else
            factory = new ConcreteFactory2();

        AbstractProductA productA = factory.createProductA();
        AbstractProductB productB = factory.createProductB();

        productB.setProductA(productA);
    }

}