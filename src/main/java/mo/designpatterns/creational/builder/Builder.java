package mo.designpatterns.creational.builder;


public interface Builder {

    Builder buildPartA();

    Builder buildPartB();

    Builder buildPartC();

    Product getResult();
}


class Product {

    Product() { }

    Product(Object partA, Object partB, Object partC) {  }

    void setPartA(Object partA) { }

    void setPartB(Object partB) { }

    void setPartC(Object partC) { }
}


class ConcreteBuilder1 implements Builder {

    private Object partA;
    private Object partB;
    private Object partC;

    @Override
    public Builder buildPartA() {
        partA = new Object();
        return this;
    }

    @Override
    public Builder buildPartB() {
        partB = new Object();
        return this;
    }

    @Override
    public Builder buildPartC() {
        partC = new Object();
        return this;
    }

    @Override
    public Product getResult() {
        return new Product(this.partA, this.partB, this.partC);
    }
}


class ConcreteBuilder2 implements Builder {

    private Product product;

    ConcreteBuilder2() {
        this.product = new Product();
    }

    @Override
    public Builder buildPartA() {
        product.setPartA(new Object());
        return this;
    }

    @Override
    public Builder buildPartB() {
        product.setPartB(new Object());
        return this;
    }

    @Override
    public Builder buildPartC() {
        product.setPartC(new Object());
        return this;
    }

    @Override
    public Product getResult() {
        Product result = product;
        product = new Product();
        return result;
    }
}


class Director {

    public Product construct(int type) {
        Builder builder = null;

        if (type == 1)
            builder = new ConcreteBuilder1();
        else
            builder = new ConcreteBuilder2();

        return builder.buildPartA().buildPartB().buildPartC().getResult();
    }
}