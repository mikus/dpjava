package mo.designpatterns.creational.factorymethod;


public abstract class Creator {

    public void create() {
        Product product = factoryMethod();
    }

    public abstract Product factoryMethod();
}


interface Product { }

class ConcreteProduct implements Product { }


class ConcreteCreator extends Creator {

    @Override
    public Product factoryMethod() {
        return new ConcreteProduct();
    }
}