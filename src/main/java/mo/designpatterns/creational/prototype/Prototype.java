package mo.designpatterns.creational.prototype;


public interface Prototype {
    Prototype clone();
}


class ConcretePrototype1 implements Prototype, Cloneable {

    @Override
    public Prototype clone() {
        try {
            return (Prototype)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}


class ConcretePrototype2 implements Prototype {

    private Object field2;

    @Override
    public Prototype clone() {
        ConcretePrototype2 copy = new ConcretePrototype2();
        copy.field2 = this.field2;
        return copy;
    }
}