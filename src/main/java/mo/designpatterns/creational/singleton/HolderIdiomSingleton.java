package mo.designpatterns.creational.singleton;



public class HolderIdiomSingleton {

    private HolderIdiomSingleton() { }


    private static class SingletonHolder {
        private static final HolderIdiomSingleton INSTANCE = new HolderIdiomSingleton();
    }


    public static HolderIdiomSingleton getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
