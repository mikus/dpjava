package mo.designpatterns.structural.adapter;


interface Adaptee1 {
    void method1();
}

interface Adaptee2 {
    void method2();
}


public class ClassAdapter implements Adaptee1, Adaptee2, Target {

    @Override
    public void method1() {

    }

    @Override
    public void method2() {

    }

    @Override
    public void doWork() {
        this.method1();
        this.method2();
    }
}
