package mo.designpatterns.structural.adapter;



class Adaptee {
    void method() { }
}


public class ObjectAdapter implements Target {

    private Adaptee adaptee;

    public void doWork() {
        adaptee.method();
    }
}
