package mo.designpatterns.structural.adapter;

/**
 * Created by mikus on 02.02.15.
 */
interface Target {
    void doWork();
}
