package mo.designpatterns.structural.bridge;


abstract class Abstraction {

    protected Implementor implementor;

    protected Abstraction(Implementor implementor) {
        this.implementor = implementor;
    }

    public abstract void doSomething();
}


class RefinedAbstraction1 extends Abstraction {

    public RefinedAbstraction1(Implementor implementor) {
        super(implementor);
    }

    @Override
    public void doSomething() {
        implementor.doWork1();
    }
}


class RefinedAbstraction2 extends Abstraction {

    public RefinedAbstraction2(Implementor implementor) {
        super(implementor);
    }

    @Override
    public void doSomething() {
        implementor.doWork2();
    }
}



interface Implementor {

    void doWork1();

    void doWork2();
}


class ConcreteImplementorA implements Implementor {

    @Override
    public void doWork1() { }

    @Override
    public void doWork2() { }
}


class ConcreteImplementorB implements Implementor {

    @Override
    public void doWork1() { }

    @Override
    public void doWork2() { }
}



class Client {

    public void doSomething() {
        Abstraction[] abstractions = new Abstraction[] {
                new RefinedAbstraction1(new ConcreteImplementorA()),
                new RefinedAbstraction1(new ConcreteImplementorB()),
                new RefinedAbstraction2(new ConcreteImplementorA()),
                new RefinedAbstraction2(new ConcreteImplementorB())
        };

        for (Abstraction abstraction : abstractions)
            abstraction.doSomething();
    }
}