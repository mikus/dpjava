package mo.designpatterns.structural.composite;

import java.util.ArrayList;
import java.util.List;


interface Component {

    void doWork();

}


class Leaf1 implements Component {

    @Override
    public void doWork() { }
}

class Leaf2 implements Component {

    @Override
    public void doWork() { }
}


public class Composite implements Component {

    private List<Component> children = new ArrayList<>();

    @Override
    public void doWork() {
        for (Component child : children)
            child.doWork();
    }

    public void add(Component child) {
        children.add(child);
    }

    public void remove(Component child) {
        children.remove(child);
    }
}



class Client {

    public void doSomething() {
        Composite composite = new Composite();

        composite.add(new Leaf1());
        composite.add(new Leaf2());

        composite.doWork();
    }
}