package mo.designpatterns.structural.decorator;


interface Component {

    void doWorkA();

    void doWorkB();
}


class ConcreteComponent implements Component {

    @Override
    public void doWorkA() { }

    @Override
    public void doWorkB() { }
}


public abstract class Decorator implements Component {

    protected Component component;

    public void doWorkA() {
        component.doWorkA();
    }

}


class ConcreteDecorator extends Decorator {

    public ConcreteDecorator(Component component) {
        this.component = component;
    }

    @Override
    public void doWorkB() {
        System.out.println("before work B");

        component.doWorkB();

        System.out.println("after work B");
    }

    public void doWorkC() { }
}


class Client {

    public void doSomething(Component component) {
        Decorator decorator = new ConcreteDecorator(component);

        decorator.doWorkA();
        decorator.doWorkB();
    }
}