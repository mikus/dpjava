package mo.designpatterns.structural.facade;


public class Facade {

    private SubSystem1 subSytem1;
    private SubSystem2 subSytem2;
    private SubSystem3 subSytem3;


    public void doWork() {
        subSytem1.doWork1();
        subSytem2.doWork2();
        subSytem3.doWork3();
    }
}


class Client {

    private Facade facade;

    public void doSomething() {
        facade.doWork();
    }

}



class SubSystem1 {

    public void doWork1() { }

}

class SubSystem2 {

    public void doWork2() { }

}

class SubSystem3 {

    public void doWork3() { }

}
