package mo.designpatterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;


class Subject {

    private Object property;

    Subject(Object property) {
        this.property = property;
    }

    public void doSomething() { }

}



class FactoryAndCache {

    private Map<Object, Subject> items = new HashMap<>();

    Subject lookup(Object property) {
        if (!items.containsKey(property))
            items.put(property, new Subject(property));
        return items.get(property);
    }

}


class Client {

    private FactoryAndCache fac = new FactoryAndCache();

    public void doSomething() {
        fac.lookup("one").doSomething();
        fac.lookup("two").doSomething();
        fac.lookup("three").doSomething();
        fac.lookup("three").doSomething();
        fac.lookup("one").doSomething();
    }
}