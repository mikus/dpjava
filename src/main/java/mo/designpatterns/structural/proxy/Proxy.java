package mo.designpatterns.structural.proxy;



interface Subject {

    void doWork();

}


class RealSubject implements Subject {

    @Override
    public void doWork() { }
}


public class Proxy implements Subject {

    private RealSubject realSubject;

    @Override
    public void doWork() {
        if (realSubject == null)
            realSubject = new RealSubject();
        realSubject.doWork();
    }
}
